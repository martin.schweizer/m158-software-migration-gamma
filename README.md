# GAMMA


## ✨ Set up the MySql Database

**Note:** Make sure your Mysql server is properly installed and accessible an you can acces the Database.
If you need to install MySQL please follow the standard follow up of MySql Community Edition (https://www.mysql.com/products/community/)


> **Step 1** - Create the MySql Database to be used by the app

- `Create a new MySql` database
- `Create a new user` and assign full privilegies (read/write)

<br />

> **Step 2** - Edit the `.env` to match your MySql DB credentials. Make sure `DB_ENGINE` is set to `mysql`.

- `DB_ENGINE`  : `mysql` 
- `DB_NAME`    : default value = `gamma`
- `DB_HOST`    : default value = `localhost`
- `DB_PORT`    : default value = `3306`
- `DB_USERNAME`: default value = `db_usr`
- `DB_PASS`    : default value = `pass`

<br />

Here is a sample:  

```txt
# .env sample

DEBUG=False                   # False enables the MySql Persistence

DB_ENGINE=mysql               # Database Driver
DB_NAME=gamma                 # Database Name
DB_USERNAME=db_usr            # Database User
DB_PASS=STRONG_PASS_HERE      # Password 
DB_HOST=localhost             # Database HOST, default is localhost 
DB_PORT=3306                  # MySql port, default = 3306 
```

<br />


## ✨ How to use it

> Download the code 

```bash
$ # Get the code
$ git clone https://gitlab.com/martin.schweizer/m158-software-migration-gamma.git
$ cd gamma
```

<br />

### 👉 Set Up for `Unix`, `MacOS` 

> Install modules via `VENV`  

```bash
$ python -m venv venv
$ source venv/bin/activate
$ pip3 install -r requirements.txt
```

<br />

> Set Up Flask Environment

```bash
$ export FLASK_APP=run.py
$ export FLASK_ENV=development
```

<br />

> Initialise the Database

```bash
$ flask db stamp head
$ flask db migrate
$ flask db update
```
<br />

> Start the app

```bash
$ flask run
```

At this point, the app runs at `http://127.0.0.1:5000/`. This is your local host!
If you have a headless server without Browser enshure to access the server by its IP-Address.

> Note
> ----
> Working with Virtualbox or VMWare as Servervirtualisation might need to change
> your Networkbinding to NAT.
> See `https://www.virtualbox.org/manual/ch06.html#natforward`for further informations.

<br />

### 👉 Set Up for `Windows` 

> Install modules via `VENV` (windows) 

```
$ python -m venv venv
$ .\venv\Scripts\Activate.ps1
$ pip3 install -r requirements.txt
```

<br />


> Set Up Flask Environment

```bash
$ # CMD 
$ set FLASK_APP=run.py
$ set FLASK_ENV=development
$
$ # Powershell
$ $env:FLASK_APP = ".\run.py"
$ $env:FLASK_ENV = "development"
```
<br />

> Initialise the Database

```bash
$ flask db stamp head
$ flask db migrate
$ flask db update
```

<br />

> Start the app

```bash
$ flask run
```


<br />

> Start the app

```bash
$ flask run
```

At this point, the app runs at `http://127.0.0.1:5000/`. This is your local host!
If you have a headless server without Browser enshure to access the server by its IP-Address.

> Note
> ----
> Working with Virtualbox as Servervirtualisation you might need to change
> your Networkbinding to NAT.
> See `https://www.virtualbox.org/manual/ch06.html#natforward`for further informations.


<br />

### 👉 Create Users

By default, the app redirects guest users to authenticate. In order to access the private pages, follow this set up: 

- Start the app via `flask run`
- Access the `registration` page and create a new user:
  - `http://127.0.0.1:5000/register`
- Access the `sign in` page and authenticate
  - `http://127.0.0.1:5000/login`

<br />

## ✨ Code-base structure

The project is coded using blueprints, app factory pattern, dual configuration profile (development and production) and an intuitive structure presented bellow:

```bash
< PROJECT ROOT >
   |
   |-- apps/
   |    |
   |    |-- home/                           # A simple app that serve HTML files
   |    |    |-- routes.py                  # Define app routes
   |    |    |-- models.py                  # Defines models  
   |    |
   |    |-- authentication/                 # Handles auth routes (login and register)
   |    |    |-- routes.py                  # Define authentication routes  
   |    |    |-- models.py                  # Defines models  
   |    |    |-- forms.py                   # Define auth forms (login and register) 
   |    |
   |    |-- static/
   |    |    |-- <css, JS, images>          # CSS files, Javascripts files
   |    |
   |    |-- templates/                      # Templates used to render pages
   |    |    |-- includes/                  # HTML chunks and components
   |    |    |    |-- navigation.html       # Top menu component
   |    |    |    |-- sidebar.html          # Sidebar component
   |    |    |    |-- footer.html           # App Footer
   |    |    |    |-- scripts.html          # Scripts common to all pages
   |    |    |
   |    |    |-- layouts/                   # Master pages
   |    |    |    |-- base-fullscreen.html  # Used by Authentication pages
   |    |    |    |-- base.html             # Used by common pages
   |    |    |
   |    |    |-- accounts/                  # Authentication pages
   |    |    |    |-- login.html            # Login page
   |    |    |    |-- register.html         # Register page
   |    |    |
   |    |    |-- home/                      # UI Kit Pages
   |    |         |-- index.html            # Index page
   |    |         |-- 404-page.html         # 404 page
   |    |         |-- *.html                # All other pages
   |    |    
   |  config.py                             # Set up the app
   |    __init__.py                         # Initialize the app
   |
   |-- requirements.txt                     # App Dependencies
   |
   |-- .env                                 # Inject Configuration via Environment
   |-- run.py                               # Start the app - WSGI gateway
   |
   |-- ************************************************************************
```
