# -*- encoding: utf-8 -*-

from apps import db

class Players(db.Model):

    __tablename__ = 'Players'

    id = db.Column(db.Integer, primary_key=True)
    player = db.Column(db.String(64), unique=True)
    first = db.Column(db.String(64), unique=False)
    last = db.Column(db.String(64), unique=False)
    street = db.Column(db.String(64), unique=False)
    plz = db.Column(db.Integer, unique=False)
    city = db.Column(db.String(64), unique=False)
    country = db.Column(db.String(64), unique=False)
    email = db.Column(db.String(64), unique=False)

    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            if hasattr(value, '__iter__') and not isinstance(value, str):
                value = value[0]

            setattr(self, property, value)

    def __repr__(self):
        return str(self.player)

class Results(db.Model):

    __tablename__ = 'Results'

    id = db.Column(db.Integer, primary_key=True)
    player = db.Column(db.Integer, unique=False)
    game = db.Column(db.Integer, unique=False)
    level = db.Column(db.Integer, unique=False)
    score = db.Column(db.Integer, unique=False)
   
    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            if hasattr(value, '__iter__') and not isinstance(value, str):
                value = value[0]

            setattr(self, property, value)

    def __repr__(self):
        return str(self.player)

class Games(db.Model):

    __tablename__ = 'Games'

    id = db.Column(db.Integer, primary_key=True)
    game = db.Column(db.String(64), unique=True)
    desc = db.Column(db.String(64), unique=False)
   
    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            if hasattr(value, '__iter__') and not isinstance(value, str):
                value = value[0]

            setattr(self, property, value)

    def __repr__(self):
        return str(self.game)

class Levels(db.Model):

    __tablename__ = 'Levels'

    id = db.Column(db.Integer, primary_key=True)
    game = db.Column(db.Integer, unique=False)
    level = db.Column(db.String(64), unique=False)
    desc = db.Column(db.String(64), unique=False)
    
    def __init__(self, **kwargs):
        for property, value in kwargs.items():
            if hasattr(value, '__iter__') and not isinstance(value, str):
                value = value[0]

            setattr(self, property, value)

    def __repr__(self):
        return str(self.desc)


