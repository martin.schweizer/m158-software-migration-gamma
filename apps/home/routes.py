# -*- encoding: utf-8 -*-

from apps.home import blueprint
from flask import render_template, request
from flask_login import login_required
from jinja2 import TemplateNotFound

from apps.home.models import Players, Results, Games, Levels


@blueprint.route('/index')
@login_required
def index():

    return render_template('home/index.html', segment='index')

@blueprint.route('/players')
@login_required
def players():
    players = Players.query
    return render_template('home/players.html', title='Players Table',players=players)

@blueprint.route('/results')
@login_required
def results():
    results = Results.query
    return render_template('home/results.html', title='Results Table',results=results)

@blueprint.route('/games')
@login_required
def games():
    games = Games.query
    return render_template('home/games.html', title='Results Table',games=games)

@blueprint.route('/levels')
@login_required
def levels():
    levels = Levels.query
    return render_template('home/levels.html', title='Results Table',levels=levels)

@blueprint.route('/about')
def about():
    
    return render_template('home/about.html', title='Results Table')

@blueprint.route('/contact')
def contact():
    
    return render_template('home/contact.html', title='Results Table')

@blueprint.route('/terms')
def terms():
    
    return render_template('home/terms.html', title='Results Table')


'''

@blueprint.route('/<template>')
@login_required
def route_template(template):

    try:

        if not template.endswith('.html'):
            template += '.html'

        # Detect the current page
        segment = get_segment(request)

        # Serve the file (if exists) from app/templates/home/FILE.html
        return render_template("home/" + template, segment=segment)

    except TemplateNotFound:
        return render_template('home/page-404.html'), 404

    except:
        return render_template('home/page-500.html'), 500


# Helper - Extract current page name from request
def get_segment(request):

    try:

        segment = request.path.split('/')[-1]

        if segment == '':
            segment = 'index'

        return segment

    except:
        return None

'''